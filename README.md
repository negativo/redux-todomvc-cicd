# Davide Berardi - 31/05/2019

## Installation

Run the application using docker-compose locally:

- `docker-compose up`
- navigate to http://localhost:3000

The application supports hot-reloading, change something in src folder to see the change happen immediately.

## Environments

The production app is at http://todomvc-prod.s3-website.eu-west-2.amazonaws.com/ and will be updated after a merge.
The production coverage reports is at http://coverage-master.s3-website.eu-west-2.amazonaws.com/

Other environments will be created at merge_requests creation, and the links will be listed in the merge request page.

## Checklist

- [v] Can run app within Docker without installing Node.js on host
- [v] Review App for app
- [v] Review App for test code coverage report

## Demo

**Merge Request:** https://gitlab.com/negativo/redux-todomvc-cicd/merge_requests/3

**App Review App:** http://b35eea38.s3-website.eu-west-2.amazonaws.com/

**Coverage Review App:** http://coverageupdate-title.s3-website.eu-west-2.amazonaws.com/

**Production App:** http://todomvc-prod.s3-website.eu-west-2.amazonaws.com/

**Coverage Production App:** http://coverage-master.s3-website.eu-west-2.amazonaws.com/

## Features

- Only one command to run a development environment.
- Local development happens on top of a production image.
- Pipeline run only for master and merge requests.
- Self-cleaning CI/CD, the s3 buckets are set to be deleted when the action `stop` is triggered.
- GitLab pages are used to host the Coverage reports.

## Security

Security concerns:

- that have been addressed

  - No hard-coding credentials. Secrets are hidden.
  - Repeatable builds, pinned images versions

- that have _not_ been addressed
  - base images security
  - enforce verified/signed commits (gpg)
  - containers monitoring

## Improvements

- Pipeline for deployment to production
- Tag releases and deploy strategy
- Node Packages verification and approval system
- Add code linters in the CI pipeline
- Implement Git Flow/GitHub Flow and protected branches (e.g. develop and master)
- `npm run eject` for the App and change the location for `index.html` from `public` to something else, since `public` is also used by GitLab pages. Could be confusing to use the same folder.
- cache the `node_modules` folder in .gitlab-ci.yml

## Other notes

- The script parts of .gitlab-ci.yml could be split in `.sh` files, is this way the scripts could be re-used on different pipelines providers.
