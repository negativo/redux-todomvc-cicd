FROM node:12.2.0-alpine as build

RUN addgroup todomvc && \
    adduser -k /dev/null -D -s /bin/sh -G todomvc -h /app todomvc
RUN ls
COPY ./package.json /app/package.json
COPY ./src /app/src
COPY ./public /app/public

USER todomvc

WORKDIR /app

RUN npm install -f --silent
ENV PATH /app/node_modules/.bin:$PATH
RUN npm run build

# production environment
FROM nginx:1.16.0-alpine
COPY --from=build /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]